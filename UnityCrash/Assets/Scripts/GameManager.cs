﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour {

	// StageGeneratorオブジェクト格納
	StageGenerator _stageGenerator;

	// パワーゲージをインスペクタから取得
	public Slider powerGuage;
	// スコアのテキスト
	public Text score;
	// ボールオブジェクト
	public GameObject ball;
	// 落下判定用のオブジェクト
	public GameObject fallCollider;
	// UIのCanvasを取得
	public Canvas canvas;
	// TitlePanelを取得
	public GameObject TitlePanel;
	// GamePanelを取得
	public GameObject GamePanel;
	// ResultPanelを取得
	public GameObject ResultPanel;
	// 今のプレイスコア
	public Text ResultScore;
	// ハイスコア
	public Text ResultHighScore;

	// 現在のパワー値
	float power = 0;
	// ボールの飛距離
	float ballPosX;
	// ボールの速度
	float speed;
	// 現在のハイスコア
	float highScore = 0f;
	// ゲーム状態遷移 Title,Start,Flying,Result
	public static string state;
	// 発射済みか否か
	public static bool isShoot;

	void Awake(){
		Application.targetFrameRate = 60;

		_stageGenerator = GameObject.Find ("StageGenerator").GetComponent<StageGenerator> ();
	}
	
	void Start () {
		state = "Title";
		isShoot = false;
		OpenTitle ();
		CloseGame ();
		CloseResult ();
	}

	void Update () {
		// ボールの位置を取得してスコアに反映
		ballPosX = ball.GetComponent<Transform>().position.x;
		score.text = "Score:" + ballPosX.ToString("N2") + "m";// スコアのテキスト

		// ボールの速度を取得
		speed = ball.GetComponent<Rigidbody>().velocity.x;


		if (state == "Title") {

		}
		else if (state == "Start"){
			// Power増減
			power = Mathf.PingPong(Time.time * 1.0f, 1.0f);
			powerGuage.value = power;

			if (Math.Abs (speed) > 0.1f) {

				state = "Flying";
			}
		}
		else if(state == "Flying"){
			if (Math.Abs (speed) <= 0f && isShoot == true) {
				
				Result ();
			}
		}
		else if(state == "Result"){

		}
	}
		
	public void Game(){
		state = "Start";
		isShoot = false;
		CloseTitle ();
		OpenGame ();
		InitBall ();
		_stageGenerator.InitStage ();
	}

	public void Result(){
		state = "Result";
		CloseGame ();
		OpenResult ();

		if (highScore < ballPosX){
			highScore = ballPosX;
		}
		ResultScore.text = "Score:" + ballPosX.ToString ("N2") + "m";
		ResultHighScore.text = "HighScore:" + highScore.ToString ("N2") + "m";
	}

	public void CallResult(){
		Result();
	}

	public void Retry(){
		state = "Start";
		CloseResult ();
		OpenGame ();
		InitBall ();
		_stageGenerator.InitStage ();
	}

	public void BackTitle(){
		state = "Title";
		OpenTitle ();
		CloseGame ();
		CloseResult ();
	}


	void InitBall(){
		ball.GetComponent<Transform> ().position = new Vector3(0.0f, 5.0f, 0.0f);
		ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
		isShoot = false;
	}

	void OpenTitle(){
		TitlePanel.SetActive (true);
	}
	void CloseTitle(){
		TitlePanel.SetActive (false);
	}

	void OpenGame(){
		GamePanel.SetActive (true);
	}
	void CloseGame(){
		GamePanel.SetActive (false);
	}

	void OpenResult(){
		ResultPanel.SetActive (true);
	}
	void CloseResult(){
		ResultPanel.SetActive (false);
	}
}

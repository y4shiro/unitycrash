﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	// 追尾するオブジェクト
	public Transform target;
	// smoothing
	public float smoothing = 5f;
	// offset
	Vector3 offset;


	void Start () {

		offset = transform.position - target.position;
	}

	void FixedUpdate () {

//		Vector3 targetCamPos = target.position + offset;
//		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
		transform.position = new Vector3(target.position.x + 50, 35, -100);
	}
}

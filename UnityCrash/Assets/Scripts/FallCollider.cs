﻿using UnityEngine;
using System.Collections;

public class FallCollider : MonoBehaviour {

	GameObject obj;
	GameManager _gameManager;

	void Start(){
		obj = GameObject.Find ("GameManager");
		_gameManager = obj.GetComponent<GameManager>();
	}

	public void OnTriggerEnter(Collider other){
		_gameManager.Result ();
	}
}

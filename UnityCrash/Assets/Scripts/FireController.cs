﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FireController : MonoBehaviour {

	public Rigidbody ball;
	public Slider powerGuage;


	void Start () {
		
	}

	void Update () {
	
	}

	public void Fire(){
		
		if (GameManager.state == "Start"){
			ball.AddForce(new Vector3(100.0f * powerGuage.value, 100.0f * powerGuage.value, 0), ForceMode.Impulse);
			GameManager.isShoot = true;
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StageGenerator : MonoBehaviour {

	// フロアプレファブのサイズ
	const int StageTipSize = 100;
	// 現在展開しているフロア数
	int currentTipIndex;

	public Transform ball;
	public GameObject[] stageTips;
	public int startTipIndex;
	public int preInstantiate;
	public List<GameObject> generatedStageList = new List<GameObject>();
	
	void Start () {
		currentTipIndex = startTipIndex - 1;
		UpdateStage(preInstantiate);
	}

	void Update () {
		// ボールの一から現在のステージチップのインデックスを計算
		int ballPositionIndex = (int)(ball.position.x / StageTipSize);
		// 次のステージチップに入ったらステージの更新処理を行う
		if(ballPositionIndex + preInstantiate > currentTipIndex){
			UpdateStage(ballPositionIndex + preInstantiate);
		}
	}

	// 指定のIndexまでのステージチップを生成して、管理下に置く
	void UpdateStage(int toTipIndex){
		if (toTipIndex <= currentTipIndex) return;
		
		// 指定のステージチップまで作成
		for(int i = currentTipIndex + 1; i <= toTipIndex; i++){
			GameObject stageObject = GenerateStage(i);
			
			// 生成したステージチップを管理リストに追加
			generatedStageList.Add (stageObject);
		}
		
		// ステージ保持上限内になるまで古いステージを削除
		while(generatedStageList.Count > preInstantiate + 2) DestroyOldestStage();
		
		currentTipIndex = toTipIndex;
	}
	
	// 指定のインデックス位置にStageオブジェクトをランダムに生成
	GameObject GenerateStage(int tipIndex){
		
		int nextStageTip = Random.Range (0, stageTips.Length);
		
		GameObject stageObject = (GameObject)Instantiate (
			stageTips[nextStageTip],
			new Vector3(tipIndex * StageTipSize - 50, 0, 0),
			Quaternion.identity
			);
		
		return stageObject;
	}
	
	// 一番古いステージを削除
	void DestroyOldestStage(){
		GameObject oldStage = generatedStageList [0];
		generatedStageList.RemoveAt (0);
		Destroy (oldStage);
	}

	// ステージ配置の初期化
	public void InitStage(){
		GameObject DefaultStage0 = (GameObject)Instantiate (
			stageTips[0],
			new Vector3(-150, 0, 0),
			Quaternion.identity
		);
		GameObject DefaultStage1 = (GameObject)Instantiate (
			stageTips[0],
			new Vector3(-50, 0, 0),
			Quaternion.identity
		);

		generatedStageList.Add (DefaultStage0);
		generatedStageList.Add (DefaultStage1);
		Start ();
	}
}
